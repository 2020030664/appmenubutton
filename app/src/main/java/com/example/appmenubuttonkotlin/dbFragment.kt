package com.example.appmenubuttonkotlin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.appmenubuttonkotlin.database.Alumno
import com.example.appmenubuttonkotlin.database.dbAlumnos

class dbFragment : Fragment() {

    private lateinit var db: dbAlumnos
    private lateinit var btnAgrega: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnEliminar: Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtFoto: ImageView
    private lateinit var txtEspecialidad : EditText


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        iniciarComponentes(view)

        btnAgrega.setOnClickListener(View.OnClickListener {

            if (txtMatricula.text.toString().contentEquals("") ||
                txtNombre.text.toString().contentEquals("") ||
                txtDomicilio.text.toString().contentEquals("") ||
                txtEspecialidad.text.toString().contentEquals("") ){

                Toast.makeText(context, "Datos faltantes", Toast.LENGTH_SHORT).show()

            }else {


                var alumno = Alumno()
                alumno.nombre = txtNombre.text.toString()
                alumno.matricula = txtMatricula.text.toString()
                alumno.domicilio = txtDomicilio.text.toString()
                alumno.especialidad = txtEspecialidad.text.toString()
                alumno.foto = "PENDIENTE"

                var query = db.getAlumno(alumno.matricula)

                db = dbAlumnos(requireContext())
                db.openDataBase()

                if (query.id!=0){
                    db.ActualizarAlumno(alumno, alumno.matricula)
                    Toast.makeText(requireContext(), "Alumno actualizado", Toast.LENGTH_SHORT).show()
                }else {
                    var id: Long = db.InsertarAlumno(alumno)
                    txtMatricula.text.clear()
                    txtNombre.text.clear()
                    txtDomicilio.text.clear()
                    txtEspecialidad.text.clear()
                    Toast.makeText(requireContext(), "Alumno agregado", Toast.LENGTH_SHORT).show()
                }


            }

        })

        btnBuscar.setOnClickListener(View.OnClickListener {

            if (txtMatricula.text.contentEquals("")){
                Toast.makeText(context, "Matricula faltante", Toast.LENGTH_SHORT).show()

            }else {
                db = dbAlumnos(requireContext())
                db.openDataBase()
                var alumno:Alumno = Alumno()
                alumno = db.getAlumno(txtMatricula.text.toString())
                if (alumno.id!=0){
                    txtNombre.setText(alumno.nombre)
                    txtDomicilio.setText(alumno.domicilio)
                    txtEspecialidad.setText(alumno.especialidad)
                    Toast.makeText(context, "Alumno Encontrado", Toast.LENGTH_SHORT).show()

                }else Toast.makeText(context, "Alumno no existe", Toast.LENGTH_SHORT).show()

            }


        })
        btnEliminar.setOnClickListener {
            if (txtMatricula.text.isEmpty()) {
                Toast.makeText(context, "Ingrese matricula ", Toast.LENGTH_SHORT).show()
            } else {
                db = dbAlumnos(requireContext())
                db.openDataBase()

                val alumno = db.getAlumno(txtMatricula.text.toString())

                if (alumno.id != 0) {
                    val builder = AlertDialog.Builder(requireContext())
                    builder.setTitle("Borrar alumno")
                    builder.setMessage("¿Esta seguro?")
                    builder.setNegativeButton("NO") { dialog, which ->
                        dialog.dismiss()
                    }
                    builder.setPositiveButton("SI") { dialog, which ->
                        db.BorrarAlumno(alumno.matricula)
                        db.close()
                        Toast.makeText(context, "Alumno Eliminado", Toast.LENGTH_SHORT).show()

                        // Limpiar campos después de borrar
                        txtMatricula.text.clear()
                        txtNombre.text.clear()
                        txtDomicilio.text.clear()
                        txtEspecialidad.text.clear()
                    }

                    builder.show()
                } else {
                    txtMatricula.text.clear()
                    txtNombre.text.clear()
                    txtDomicilio.text.clear()
                    txtEspecialidad.text.clear()
                    Toast.makeText(context, "No existe el alumno", Toast.LENGTH_SHORT).show()
                }
            }
        }




        return view
    }

    fun iniciarComponentes(view: View){
        btnAgrega = view.findViewById(R.id.btnAgregar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnEliminar = view.findViewById(R.id.btnBorrar)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        txtFoto = view.findViewById(R.id.txtFoto)


    }
}
