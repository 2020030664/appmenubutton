package com.example.appmenubuttonkotlin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    private lateinit var txtAlumno : TextView
    private lateinit var txtMatricula : TextView
    private lateinit var txtTel : TextView
    private lateinit var txtCorreo : TextView
    private lateinit var txtCarrera : TextView

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        txtAlumno = view.findViewById(R.id.txtAlumno)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtCarrera = view.findViewById(R.id.txtCarrera)
        txtTel = view.findViewById(R.id.txtTelefono)
        txtCorreo = view.findViewById(R.id.txtCorreo)

        txtAlumno.text = getString(R.string.nombreAlumno)
        txtMatricula.text = getString(R.string.matricula)
        txtCarrera.text = getString(R.string.nombreCarrera)
        txtCorreo.text = getString(R.string.Correo)
        txtTel.text = getString(R.string.Telefono)


        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}