package com.example.appmenubuttonkotlin.database

import android.provider.BaseColumns

class DefinirTabla {

    // Clase Estatica, no genera objetos, hace referencia con el nombre de la clases y atributos
    object Alumnos : BaseColumns{

        const val TABLA = "alumnos"
        const val ID = "id"
        const val MATRICULA = "matricula"
        const val NOMBRE = "nombre"
        const val ESPECIALIDAD = "especialidad"
        const val DOMICILIO = "domicilio"
        const val FOTO = "foto"

    }
}
